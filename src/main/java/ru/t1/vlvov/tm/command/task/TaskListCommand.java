package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Display all tasks.";

    @NotNull
    private final String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("SELECT SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortName = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortName);
        @Nullable final String userId = getAuthService().getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}