package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IRepository;
import ru.t1.vlvov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        @NotNull final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

}